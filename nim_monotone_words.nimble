# Package

version       = "0.1.0"
author        = "joachimschmidt557"
description   = "Filtering words with alphabetically increasing characters"
license       = "MIT"
srcDir        = "src"
bin           = @["nim_monotone_words"]



# Dependencies

requires "nim >= 0.20.9"
