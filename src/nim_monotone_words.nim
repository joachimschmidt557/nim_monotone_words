import parseopt, sets, strutils

proc isMonotone(word:string):bool =
  for i in word.low .. word.high - 1:
   if word[i] > word[i+1]:
     return false
  return true

proc main() =
  var
    p = initOptParser()
    texts:seq[string]
    dictionary = initHashSet[string]()

  # Parse arguments
  while true:
    p.next()
    case p.kind
    of cmdEnd: break
    of cmdShortOption, cmdLongOption:
      if p.key == "t" or p.key == "text":
        texts.add(readFile(p.val))
    of cmdArgument:
      discard

  # Generate matrix
  for text in texts:
    for word in text.split():
      dictionary.incl(word.toLower)

  for word in dictionary:
    if word.isMonotone:
      echo word

when isMainModule:
  main()
